package com.eauction.buyer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;

@SpringBootApplication
@EnableKafka
public class BuyerBidConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BuyerBidConsumerApplication.class, args);
	}

}
