package com.eauction.buyer.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class BuyerBidUpdate implements Serializable {
    private String productId;
    private String bidAmount;
    private String email;
}