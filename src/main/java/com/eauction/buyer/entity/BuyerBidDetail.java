package com.eauction.buyer.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class BuyerBidDetail implements Serializable {
    private String productId;
    private String bidAmount;
    private String firstName;
    private String lastName;
    private String address;
    private String city;
    private String state;
    private String pin;
    private String phone;
    private String email;
}