package com.eauction.buyer.service;

import com.eauction.buyer.entity.BuyerBidDetail;
import com.eauction.buyer.entity.BuyerBidUpdate;
import com.eauction.buyer.repository.BidDetailsRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Log4j2
@RequiredArgsConstructor
@Service
public class BidKafkaConsumerService {

    @Autowired
    private final BidDetailsRepository bidDetailsRepository;

    @KafkaListener(topics = "bid-topic", groupId = "group_id")
    public void consume(BuyerBidDetail buyerBidDetail) {
        log.info("Consumer receiving bid: {}", buyerBidDetail);
        bidDetailsRepository.placeBid(buyerBidDetail);
        log.info("Consumer created bid");
    }

    @KafkaListener(topics = "bid-update-topic", groupId = "group_id")
    public void consumeUpdate(BuyerBidUpdate buyerBidUpdate) {
        log.info("Consumer receiving bid: {}", buyerBidUpdate);
        bidDetailsRepository.updateBid(buyerBidUpdate.getProductId(),buyerBidUpdate.getEmail(),
                buyerBidUpdate.getBidAmount());
        log.info("Consumer updated bid");
    }
}
